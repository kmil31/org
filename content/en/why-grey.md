---
title: Why 'Grey' Software?
description: Curious about why we call our organization Grey Software?
category: Info
position: 4
---

What does it mean for software to be Grey?

What does it mean for anything to be Grey?

Well, to understand the Grey, we must first observe that we live in a world of polarities.

Progressivism and Conservatism.

Introversion and Extroversion.

Idealism and Pragmatism.

Black and White.

Polarities like these exist all around us, and without finding balance and harmony between them, we risk creating an
increasingly divided world with less understanding and more suffering.

To be Grey is to seek balance and harmony with the polarities we encounter.

In the spirit of balance and harmony, we define **Grey Software** as:

Software you can trust, love, and learn from.

### Software you can Trust

Transparency builds trust, which is why we make our designs, code, accounting, and analytics open to the public.

### Software you can Love

Software is a part of our human ecology now, so we value creating delightful software experiences that people can love
and enjoy.

### Software you can Learn From

We empower future software developers by writing our code to be easily understood, and coupling it with open educational
resources.

<br />

<details class="my-6">

 <summary class="text-3xl hover:cursor-pointer">Learn More</summary>
    
    
### Grey Software is open source

We believe that gathering valuable knowledge and insight from the largest community of minds helps us orient ourselves
towards the Grey.

By following the open-source philosophy and making our code transparently available for public viewing, we can engage
the large community of minds we seek.

### Grey Software is coherent and educational

Humans write and rewrite code. Other humans take charge if something happens to the original author(s) of some code.

However, new authors need to understand the codebase before they can contribute. That's why we write our code in a way
that can be easily explained, and couple it with educational resources and documentation.

To empower the next generation to continue our mission, we're democratizing access to high-quality software education so
learners worldwide can educate themselves with basic hardware and internet.

### Grey Software is not created "For-Profit"

Many of the companies that create the software we rely on everyday are for-profit corporations.

The [US Chamber of Commerce](https://www.uschamber.com/co/start/strategy/nonprofit-vs-not-for-profit-vs-for-profit)
defines a for-profit corporation as one that operates with the goal of making money.

The business owner earns an income from the company and may also pay shareholders and investors from the profits.

For us, creating software with the fundamental legal obligation of making money is not aligned with a balanced
orientation.

We are not against generating revenue, but we believe that our foundation should be built on purpose rather than profit.

Without balance in the [profit-purpose polarity](https://www.youtube.com/watch?v=MyrhP9eXFNw), companies find themselves
exploiting the very people they initially set out to serve.

![](https://i.imgur.com/ClGol38.png)

That is why we are **NOT** a "for-profit" company both at our personal and legal cores.

We are registered as a not-for-profit organization in Canada, where NFPOs and Charities are different org structures.

![](https://i.imgur.com/0NTc3YL.jpg)

 </details>
