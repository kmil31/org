---
title: Focused Browsing
description: Reclaim your focus with a web extension that hides distracting feeds!
category: App Collection
position: 9
---

![Focused Browsing Promo](/focused-browsing/promo.png)

**Reclaim your focus with a web extension that hides distracting feeds!**

<extension-links chrome="https://chrome.google.com/webstore/detail/ocbkghddheomencfpdiblibbjhjcojna" firefox="https://addons.mozilla.org/en-US/firefox/addon/focused-browsing/"></extension-links>
<br/>

## Features

### Show & Hide distractions without leaving the tab you're on.

We want to empower you to focus when you want to, but we also don't want to make it tedious for you when you want to browse what's happening on the Internet.

That is why we made it easy for you to hide and bring back feeds without leaving your tab.

![Screenshot showcasing hiding distractions without leaving the tab you're on](/focused-browsing/screenshot-1.png)

### Control Distractions using keyboard shortcuts

We added keyboard shortcuts to make it seamless and intuitive for you to toggle distractions on our supported websites.

`LeftShift + RightShift` currently toggles all distractions, and we are thinking of other shortcuts to help optimize & personalize your experience.

![Screenshot showcasing controlling focus using keyboard shortcuts](/focused-browsing/screenshot-2.png)

### We support Dim and Dark modes

We added dim and dark mode support for Twitter because we wanted our extension to fit seamlessly with someone's browsing experience on the site.

![Screenshot showcasing Dim and Dark mode support](/focused-browsing/screenshot-3.png)

## Try Focused Browsing Today!

You can download the latest release of Focused Browsing from the Chrome or Firefox extension marketplaces!

<extension-links chrome="https://chrome.google.com/webstore/detail/ocbkghddheomencfpdiblibbjhjcojna" firefox="https://addons.mozilla.org/en-US/firefox/addon/focused-browsing/"></extension-links>
<br/>

### Preview Releases

If you would like early access to preview releases of Focused Browsing, we also publish our extension on Github!

<cta-button link="https://github.com/grey-software/focused-browsing/releases" text="Get Early Access"></cta-button>

## Compatibility Chart

### Legend

**✅ Passed**
**❗️Unsupported**
**❓Untested**

| Operating System | ![Brave](https://raw.githubusercontent.com/alrra/browser-logos/master/src/brave/brave_24x24.png) | ![Chrome](https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_24x24.png) | ![Firefox](https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_24x24.png) | ![Edge](https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_24x24.png) |
| ---------------- | ------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------- |
| OSX 11.4 (20F71) | 1.26.74 ✅                                                                                       | 91.0.4472.114 ✅                                                                                    | ✅                                                                                                     | ❓                                                                                            |

<br>

## Project Status

This project is currently being maintained by [Arsala](https://gitlab.com/ArsalaBangash), [Avi](https://gitlab.com/daveavi), and [Raj](https://gitlab.com/teccUI).

## Credits and Gratitude

### News Feed Eradicator by Jordan West

Focused Browsing originally started as two separate student projects called Twitter Focus and LinkedIn Focus. Both of these were inspired by [News Feed Eradicator for Facebook](https://github.com/jordwest/news-feed-eradicator).

We are sincerely grateful to Jordan for using the MIT license for NFE. This allowed us to learn from his codebase, and allowed us to use his collection of quotes for our MVP.

### The open source ecosystem

All software stands upon the foundations laid by the open source world.

We are where we are because of the time, energy, and passion of open source software developers around the world.

We are sincerely grateful for our access to tools that help us create better software.

</br>
