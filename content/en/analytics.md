---
title: Analytics 📈
description: Grey Software's Open Analytics
category: Info
position: 4
---

Statistics provided by ![Plausible Analytics](/tech-stack/plausible-logo.png)

## Landing Website

<iframe plausible-embed src="https://plausible.io/share/grey.software?auth=k8ybZUWqj4n7wvl26NhNI&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

**[Full Analytics](https://plausible.io/grey.software)**

## Org Website

<iframe plausible-embed src="https://plausible.io/share/org.grey.software?auth=cQh8gVI_y8KOiczjrwmfw&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

**[Full Analytics](https://plausible.io/org.grey.software)**

## Learn

<iframe plausible-embed src="https://plausible.io/share/learn.grey.software?auth=zgkaBtGCGYoRfQ_j_ZyRR&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

**[Full Analytics](https://plausible.io/learn.grey.software)**

## Resources

<iframe plausible-embed src="https://plausible.io/share/resources.grey.software?auth=Z8FYqPyCvyAbimDtTQN8K&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/resources.grey.software)

## Onboarding

<iframe plausible-embed src="https://plausible.io/share/onboarding.grey.software?auth=PPr6299z_kbD_AweU0f88&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/onboarding.grey.software)

## Glossary

<iframe plausible-embed src="https://plausible.io/share/glossary.grey.software?auth=6TBqBDbrKAcJe7xqwiI0Y&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/glossary.grey.software)

## Material Math

<iframe plausible-embed src="https://plausible.io/share/material-math.grey.software?auth=R4BQcZ1FJXAQmpkuSobSX&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/material-math.grey.software)
