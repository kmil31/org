---
title: GitLab Management 
description: Grey Software's GitLab Management 
category: Info
position: 8
--- 

At Grey Software, we use Gitlab to manage our projects and code. 

> Resources:
> - [Agile planning with GitLab](https://about.gitlab.com/solutions/agile-delivery/)
> - [How to use Gitlab](https://docs.gitlab.com/ee/topics/use_gitlab.html)
> - [How to use GitLab for Agile software development](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/)

![](https://i.imgur.com/BWv47YR.png)


## Planning and Tracking a project

### Issues

Gitlab issues are used to collaborate on ideas, solve problems, and plan work. 

This feature is used for many purposes and can easily be customized to the needs and workflow of the project.

####  How we process issues at Grey software?  

![](https://i.imgur.com/OuBpZAb.png)

At Grey Software we have developed a lifecycle for issues with the help of labels.

Each stage of the lifecycle has a specific label associated with it, which allows you to easily trace the issue's status.

###### Stage Labels. ![](https://i.imgur.com/bNDMC7g.png)

###### Status Labels. ![](https://i.imgur.com/zgwdRP4.png)


###### This issue was created to track the task of designing and engineering the Product section of our landing website. !![](https://i.imgur.com/ilOoKjM.png)


###### This issue is an 'idea' to publish a new landing website![](https://i.imgur.com/OU0xcTn.png)


###### This issue is to 'brainstorm' the code flow for unlocking premium features for Github sponsors.![](https://i.imgur.com/YQFvlVE.png)


Every project has associated issues which can be viewed all at once to track the progress of the project.


###### Issues associated to Grey Docs. ![](https://i.imgur.com/E9JaMjO.png)


#### Issue Weights

> - [Issue Weights in GitLab](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html/)

The particular way we use weights at Grey software is by breaking apart an issue into tasks, writing out those tasks as markdown checklist items, and then once we have the final weight, we assign it to an issue. 

Weights are not set in stone.

You can change the weight of an issue as you discover more about the solution, and it is also OK to assign weights retrospectively. 


#### Issue Boards

Issue boards allow us to plan, organize, and visualize a workflow for a feature or product release.

Issue boards help to visualize how issues are being processed, and are inspired by Kanban boards.


###### The following issue board exhibits:

###### * list of issues the team is working on.

###### * Team member to whom a particular issue is being assigned.

###### * Where the issues are in the workflow.

![](https://i.imgur.com/XT3WDYh.png)

### Labels

As the count of issues, merge requests, and epics grows in GitLab, it’s more and more challenging to keep track of those items. Especially as the organization grows with time. This is where labels come in. They help to organize and tag work so people can track and find the work items they are interested in.

###### Following is the list of some labels![](https://i.imgur.com/Ou1y5Ha.png)


###### Labels are a key part of issue boards. With labels epics, issues and merge requests can be easily categorized.
![](https://i.imgur.com/XFuTYge.png)


### Comments and threads

GitLab encourages communication through comments, threads, and code suggestions which makes collaboration more easier.


###### Following are the comments made under the "[We need to make tweaks to make GreyDocs theme suit our visual design](https://https://gitlab.com/grey-software/templates/grey-docs/-/issues/4)" issue  ![](https://i.imgur.com/oiaj3jg.png)



### Milestones

Milestones are a way to track issues and merge requests created to achieve a broader goal in a certain period of time.

###### It can be seen in this milestone there are 6 issues and 5 merge requests and is 16% complete. ![](https://i.imgur.com/hPEoLSs.png)

###### Further details are explained next ![](https://i.imgur.com/bWuEtLh.png)

1. Milestone Completion status
2. Due Date : when the milestone due.
3. Issue Details : no of open and closed issues.
4. Merge Request Details : no of open and closed merge requests.
5. List of on-going issue and merge requests.
6. List of completed issues and merge requests


### Iterations

Iterations are useful for planning agile or agile-like sprints to capture action items to be completed within a specific time period.

[This is the final sprint towards Grey Software v4.0](https://gitlab.com/groups/grey-software/-/iterations/32035).

![](https://i.imgur.com/7tMFlYt.png)
###### Burndown charts show the number of issues over the course of a milestone.

###### Burnup charts show the assigned and completed work for a milestone.



### Epics

**[Gitlab Documentation](https://docs.gitlab.com/ee/user/group/epics/)**

When issues in a group share a theme across projects and milestones, you can manage them by using epics.

#### The possible relationships between epics and issues are:

* An epic can be the parent of one or more issues.
* An epic can be the parent of one or more child epics

![](https://i.imgur.com/cGXWCiy.png)



###### One of the epics of Grey Software is mentioned below ![](https://i.imgur.com/pfqaOHS.png)
1. Epic title.
2. Child epics or issues: two of the child issues are associated with this epic.
3. Start date of the epic.
4. Due date of the spic.
5. Labels attached to it, we can know epic's status.
6. Parent epics: the above epic has two parent epics.

Its Hierarchy is shown below.

![](https://i.imgur.com/B3ggf7M.png)




#### What are some examples of epics in Grey Software

[Our organization's high-level objectives are listed as Gitlab Epics](https://gitlab.com/groups/grey-software/-/epics).

**🎯 [Maintain open source software that people can trust, love, and learn from](https://gitlab.com/groups/grey-software/-/epics/18)**

**🎯 [Be a global OSS Education thought leader](https://gitlab.com/groups/grey-software/-/epics/2)**

**🎯 [Generate sustainable revenue](https://gitlab.com/groups/grey-software/-/epics/16)**


#### Epic Board

Epic boards tracks the existing epics through labels. They appear as cards in vertical lists, organized by their assigned labels.

![](https://i.imgur.com/VHRtteR.png)


### Roadmaps  

Epics and milestones in a group containing a start date or due date can be visualized in a form of a timeline (that is, a Gantt chart). The Roadmap page shows the epics and milestones in a group, one of its subgroups, or a project in one of the groups.

![](https://i.imgur.com/YQDjvXZ.png)

