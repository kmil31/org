---
title: Explorers 🧭
description: Grey Software's Open Source Explorers
category: Team
postion: 13
explorers:
  - name: Jia Hong
    avatar: https://avatars.githubusercontent.com/u/47465704?s=400&u=9aa7cfe98ec20f7b33abf59e4a7dd1fa92b74b8f
    position: Explorer
    github: https://github.com/jiahongle
    gitlab: https://gitlab.com/jiahongle
    linkedin: https://linkedin.com/in/jiahongle
  - name: Agnes Lin
    avatar: https://avatars3.githubusercontent.com/u/50331796?v=3&s=200
    position: Explorer
    github: https://github.com/agnes512
    gitlab: https://gitlab.com/agnes512
    linkedin: https://www.linkedin.com/in/agnes-yuchi-lin-a4601b172/
  - name: Pranjal Verma
    avatar: https://avatars3.githubusercontent.com/pvcodes
    position: Explorer
    github: https://github.com/pvcodes
    gitlab: https://gitlab.com/pvcodes
    linkedin: https://www.linkedin.com/in/pvcodes
  - name: Kunal Verma
    avatar: https://avatars.githubusercontent.com/u/72245772?s=460&u=756170512bf927ef4410c0d94f3b630510e910cb&v=4
    position: Explorer
    github: https://github.com/Zipher007
    gitlab: https://gitlab.com/Zipher007
    linkedin: https://www.linkedin.com/in/verma-kunal/
  - name: Deepanshu Dhruw
    avatar: https://avatars.githubusercontent.com/u/55559527?s=400&u=7a06beafb54ec1d480eed23bfc3dc91a9a49bd3d
    position: Explorer
    github: https://github.com/devblin
    gitlab: https://gitlab.com/devblin
    linkedin: https://www.linkedin.com/in/devblin/
  - name: Deepak Yadav
    avatar: https://avatars.githubusercontent.com/u/66518459?s=400&u=43bed3001d147bb95a3c958e2ab032e4502f67b7&v=4
    position: Explorer
    github: https://github.com/deepak-yadavdy
    gitlab: https://gitlab.com/deepak_yadavdy
    linkedin: https://www.linkedin.com/in/deepak-yadav-/
  - name: Zaheer Abbas
    avatar: https://secure.gravatar.com/avatar/2b555f12fba4c5d6bd55a96a19463618?s=800&d=identicon
    position: Explorer
    github: https://github.com/nk4456542
    gitlab: https://gitlab.com/nk4456542
    linkedin: https://www.linkedin.com/in/zaheer-abbas-931986175/
  - name: Abhishek Kumar
    avatar: https://avatars.githubusercontent.com/u/48255244?s=460&u=34c3f74b3a931456095c677e1b41c875955b4713&v=4
    position: Explorer
    github: https://github.com/Abhishek-kumar09
    gitlab: https://gitlab.com/Abhishek-kumar09
    linkedin: https://www.linkedin.com/in/abhishek-kumar-016ba1175/
  - name: Sudhanshu Kumar
    avatar: https://avatars.githubusercontent.com/u/63813872?s=400&u=e23bd57ca2f6b8800cc035fb4fbb1f0ea2eed3c0&v=4
    position: Explorer
    github: https://github.com/SudhanshuBlaze
    gitlab: https://gitlab.com/SudhanshuBlaze
    linkedin: https://www.linkedin.com/in/SudhanshuBlaze
  - name: Rupam Shil
    avatar: https://secure.gravatar.com/avatar/44892de108258d96e067e040512de7be?s=46&d=identicon
    position: Explorer
    github: https://github.com/Rupam-Shil
    gitlab: https://gitlab.com/rupamshil111
    linkedin: https://www.linkedin.com/in/rupam-shil-74150a190/
  - name: Uzair Ali
    avatar: https://gitlab.com/uploads/-/system/user/avatar/8529524/avatar.png?width=400
    position: Explorer
    github: https://github.com/uzair-ali10
    gitlab: https://gitlab.com/uzair_ali10
    linkedin: https://www.linkedin.com/in/uzair-ali-9285261ba/
  - name: Gaurav Pandey
    avatar: https://secure.gravatar.com/avatar/f46f96c73c360d15b237eb762e171979?s=180&d=identicon
    position: Explorer
    github: https://github.com/codewithgauri
    gitlab: https://gitlab.com/codewithgauri
    linkedin: https://www.linkedin.com/in/gaurav-pandey-a5b884131/
  - name: Juan Lamilla
    avatar: https://avatars.githubusercontent.com/u/6435174?v=4
    position: Explorer
    github: https://github.com/JuanesLamilla
    gitlab: https://gitlab.com/JuanesLamilla
    linkedin: https://www.linkedin.com/in/juanlamilla/
  - name: Kamalakar Gavali
    avatar: https://secure.gravatar.com/avatar/9ddfa7e18720b702106c26607ab1807a?s=46&d=identicon
    position: Explorer
    github: https://github.com/Kamalakar-Gavali
    gitlab: https://gitlab.com/Kamalakar-Gavali
    linkedin: https://in.linkedin.com/in/kamalakar-gavali
  - name: Manas Uniyal
    avatar: https://avatars.githubusercontent.com/u/35189722?s=460&u=bce52ae7b56aa90ca9f827e5ded48aaf2d9a1cd5&v=4
    position: Explorer
    github: https://github.com/ManasUniyal
    gitlab: https://gitlab.com/ManasUniyal
    linkedin: https://www.linkedin.com/in/manasuniyal/
  - name: Zainuddeen Abdul Muiz
    avatar: https://avatars.githubusercontent.com/u/60746360?s=400&u=f94ab5e152cc6a77aa11c06c3d05c904348fd668&v=4
    position: Explorer
    github: https://github.com/Zain-Muiz
    gitlab: https://gitlab.com/Zain-Muiz
    linkedin: https://www.linkedin.com/in/zainuddeen-abdul-muiz-36969b196
  - name: Rahul Rawat
    avatar: https://avatars.githubusercontent.com/u/20657303?s=400&v=4
    position: Explorer
    github: https://github.com/raulinc
    gitlab: https://gitlab.com/raulinc
    linkedin: https://www.linkedin.com/in/rahul-rawat-618a23127
  - name: Sai Rithvik Ayithapu
    avatar: https://avatars.githubusercontent.com/u/58836321?s=460&u=97e41392c1ae6e9466b83fd0335f176f62921a62&v=4
    position: Explorer
    github: https://github.com/rithvik2607
    gitlab: https://gitlab.com/rithvik2607
    linkedin: https://www.linkedin.com/in/sai-rithvik-ayithapu-3568881a3
  - name: Riyan Ahmed
    avatar: https://avatars.githubusercontent.com/u/25188689?v=4
    position: Explorer
    github: https://github.com/riyan-ahmed
    gitlab: https://gitlab.com/riyan-ahmed
    linkedin: https://www.linkedin.com/in/riyan-ahmed/
  - name: Zamraz Arif
    avatar: https://avatars.githubusercontent.com/u/87978436?v=4
    position: Explorer
    github: https://github.com/CyberGhost317
    gitlab: https://gitlab.com/CyberGhost317
    linkedin: https://www.linkedin.com/in/zamraz-arif-k-12982618b/
  - name: Abhishek Munda
    avatar: https://s.gravatar.com/avatar/e076659c1e7748b10a0ab46ffd8d4d1d?s=80
    position: Explorer
    github: https://github.com/abhishek-munda
    gitlab: https://gitlab.com/abhishek-munda
    linkedin: www.linkedin.com/in/abhishekmunda


---

This page is for the citizens of the internet that wanted to explore the world of open source and did so by completing our [Onboarding Process](https://onboarding.grey.software)

<team-profiles :profiles="explorers"></team-profiles>
