---
title: Tech Stack 🧰
description: Grey Software's Tech Stack
category: Info
position: 7
---

## Github

![Github Preview](/tech-stack/github-preview.png)

GitHub is home to the largest software development community on the planet with over 50M+ developers and 100M+ projects. Grey software uses GitHub to manage collaborative open source software development projects and find the organization through the Github Sponsors program.

<cta-button  link="https://github.com/join" text="Sign Up" > </cta-button>

## Gitlab

![ Preview](/tech-stack/gitlab-preview.png)

Organizations rely on GitLab’s source code management, CI/CD, security, and more to deliver software rapidly. We use Gitlab along with Github because both services offer unique features that our organization takes advantage of.

<cta-button  link="https://gitlab.com/users/sign_up" text="Sign Up" > </cta-button>

<alert>
Why do we use both Github and Gitlab?
We use Gitlab for our primary development operations because we found it more conducive to handling the complexity of software project management. We use GitHub because of its vast community, classroom platform, and sponsorships program.</alert>


## Figma

![Figma Preview](/tech-stack/figma-preview.png)

Figma is a web-based graphic and user interface design app. It allows you to collaborate on design files with your team in real time, and fits very well in the toolkit of a frontend web developer. 

At Grey Software, we use Figma for our UI design and prototyping work. 

<cta-button  link="https://www.figma.com/education/" text="Claim Edu Plan!" > </cta-button>

<cta-button  link="http://figma.grey.software" text="Our Figma File" > </cta-button>

## Plausible Analytics

![Plausible Preview](/tech-stack/plausible-preview.png)

<cta-button  link="http://org.grey.software/analytics" text="Our Analytics" > </cta-button>

## HackMD

![HackMD Preview](/tech-stack/hackmd-preview.png)

HackMD allows you to work on Markdown files and collaborate with your teammates in real time. At Grey Software, we use HackMD as our Google Docs or Microsoft Word alternative for collaborating on team documents.

<cta-button link="https://hackmd.io/join" text="Sign Up" > </cta-button>

## N8N

![N8N Preview](/tech-stack/n8n-preview.png)

Workflow autmation allows our organizational processes to function with fewer errors and less time by having computers execute rule-based logic that automates manual work.

At Grey Software, we love using N8N to automate our workflows because of its fun user-interface and commitment to open-source transparency.

<cta-button link="https://n8n.io" text="Learn More" > </cta-button>


## Vue

![vue Preview](/tech-stack/vue-preview.png)

<cta-button link="https://vuejs.org" text="Learn More" > </cta-button>

## Nuxt

![Nuxt Preview](/tech-stack/nuxt-preview.png)


<cta-button link="https://nuxtjs.org" text="Learn More" > </cta-button>

## Quasar

![Quasar Preview](/tech-stack/quasar-preview.png)


<cta-button link="https://quasar.dev" text="Learn More" > </cta-button>

## Overlay

![Overlay Preview](/tech-stack/overlay-preview.png)

<cta-button link="https://overlay-tech.com" text="Learn More" > </cta-button>


## Brave Browser and Firefox

![Browsers Preview](/tech-stack/browsers-preview.png)

Most of our projects management and development happens on web browsers, and there are two major open source engines in the market right now: Chromium and Firefox Quantum.

At Grey software, we regularly use both browser engines for diverse compatability, and our favorite browsers to work with are Mozilla Firefox and Brave. 

<cta-button  link="https://brave.com/download/" text="Brave Browser" > </cta-button> <cta-button  link="https://www.mozilla.org/en-US/firefox/new/" text="Download Firefox" > </cta-button>


## Bitwarden

![Bitwarden Preview](/tech-stack/bitwarden-preview.png)


Bitwarden is a free and open-source password management service
that stores sensitive information such as website credentials in an encrypted vault.

We use Bitwarden to manage the multiple passwords and API access keys we have to create as modern software developers. If you use another password manager, you may continue to do so :)

<cta-button  link="https://vault.bitwarden.com/#/register" text="Sign Up" > </cta-button>

## VisBug

![VisBug Preview](/tech-stack/visbug-preview.png)

VisBug is an open-source web extension that allows users to interact with their webpage using powerful tools that can edit elements and inspect styles. 

Our team at Grey Software highly recommends having this extension since it has allowed us to treat the webpage as an art-board that we can edit and inspect before heading back into the code editor.

<cta-button  link="https://addons.mozilla.org/en-US/firefox/addon/visbug/" text="Firefox Add-On" > </cta-button> <cta-button  link="https://chrome.google.com/webstore/detail/visbug/cdockenadnadldjbbgcallicgledbeoc?hl=en" text="Chrome Extension" > </cta-button>
